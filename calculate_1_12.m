clear
close all
% pdf27页 书本11页
q = 1.6*10^(-19);
epsilon0 = 8.85*10^(-12);
mu0 = 4*pi*10^(-7);
mu = mu0;
% 根据附录B可知AWG 26铜线的半径
a = 2.032*10^(-4);
sigmaCu = 64.516*10^6;
% 单位长度 1米
l = 1;
% 频率范围
f = 10.^(5:0.001:9);
% 由(1.7)可知，直流电阻
RDC = l/(pi*a^2*sigmaCu);
% 由(1.9)可知，趋肤深度
delta = (pi*f*mu*sigmaCu).^(-1/2);
% 由(1.12)可知，引线的串联电阻：
Rs = RDC*a./(2*delta);

loglog(f,RDC*ones(1,length(f)));hold on;
loglog(f,Rs);hold on;
% legend('直流近似','趋肤深度近似')
grid on;
xlabel('频率f(Hz)')
ylabel('电阻R(Ω)')
%% 
% % 如果？*delta>a，那么直流近似
% % 如果？*delta<a，那么delta<<a，那么趋肤深度近似
% xuxian = Rs.*(1-~(3*delta<a)) +...
%     RDC.*(1-~(3*delta>a));
% loglog(f,xuxian);
%% 利用电流密度积分
p = (-1i*2*pi*f*mu*sigmaCu).^(1/2);
Jr = @(r) (abs(besselj(0,p.*r)).^2).*r;
% (B.15)
R = abs(p).^2./(2*pi*a^2*sigmaCu.*abs(besselj(1,p.*a)).^2).*integral(Jr,0,a,'ArrayValued',true);
loglog(f,R);
legend('直流近似','趋肤深度近似','利用电流密度积分')










