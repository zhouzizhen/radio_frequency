clear
Vg=10; % voltage amplitude
Zg=50; % generator impedance
ZL=40; % 负载阻抗
Z1=75; % 第一条线的特性阻抗
Z2=50; % 第二条线的特性阻抗
l1=0.35; % 第一条线的归一化长度
l2=0.65; % 第二条线的归一化长度
% 第二条线的输入阻抗，该阻抗是第一条线的负载
ZL1=Z2*(ZL+1i*Z2*tan(2*pi*l2))/...
    (Z2+1i*ZL*tan(2*pi*l2))
% reflection coefficient and the connection point of two line
% 反射系数和两条线的连接点
Gamma0=(ZL1-Z1)/(ZL1+Z1)
% 源反射系数
GammaS=(Zg-Z1)/(Zg+Z1)
% power
Pin=Vg^2/(8*Z1)*abs(1-GammaS)^2/...
    abs(1-GammaS*Gamma0*exp(-2*1i*2*pi*l1))^2*(1-abs(Gamma0)^2)
% 插入损耗
IL=-10*log10(1-abs(Gamma0)^2)