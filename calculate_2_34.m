clear
VG=10; % voltage amplitude
ZG=50; % generator impedance
ZL1=40; % 负载阻抗
ZL2=50; % 负载阻抗
Z0=50; % 特性阻抗
l1=0.35; % 第一条线的归一化长度
l2=2.4; % 第二条线的归一化长度
% 输入阻抗
Zin1 = Z0*(ZL1+1i*Z0*tan(2*pi*l1))/...
    (Z0+1i*ZL1*tan(2*pi*l1));
Zin1 = abs(Zin1)
Zin2 = Z0*(ZL2+1i*Z0*tan(2*pi*l2))/...
    (Z0+1i*ZL2*tan(2*pi*l2))
Ztotal = 1/(1/Zin1+1/Zin2)
% 反射系数和两条线的连接点
Gamma0=(Ztotal-Z0)/(Ztotal+Z0);
% 源反射系数
GammaS=(ZG-Z0)/(ZG+Z0);
% power
Pin=VG^2/(8*Z0)*(1-abs(Gamma0)^2)

% 交点处的电压
Vinter = (2*Ztotal*Pin)^(1/2);
Pinter = abs(Vinter)^2/(2*Ztotal);
PL1 = abs(Vinter)^2/(2*Zin1)
PL2 = abs(Vinter)^2/(2*Zin2)
P = PL1+PL2
