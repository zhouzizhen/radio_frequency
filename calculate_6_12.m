clear
close all

X_Si = 4.05;
X_GaAs = 4.07;
VM_Al = 4.28;
VM_Au = 5.1;
NC_Si = 2.8E19*1E6;
NC_GaAs = 4.7E17*1E6;
epsilonr_Si = 11.9;
epsilonr_GaAs = 13.1;

Vd_fun(VM_Al,X_Si,NC_Si)
Vd_fun(VM_Au,X_Si,NC_Si)
Vd_fun(VM_Al,X_GaAs,NC_GaAs)
Vd_fun(VM_Au,X_GaAs,NC_GaAs)

ds_fun(Vd_fun(VM_Al,X_Si,NC_Si),epsilonr_Si)*1E9
ds_fun(Vd_fun(VM_Au,X_Si,NC_Si),epsilonr_Si)*1E9
ds_fun(Vd_fun(VM_Al,X_GaAs,NC_GaAs),epsilonr_GaAs)*1E9
ds_fun(Vd_fun(VM_Au,X_GaAs,NC_GaAs),epsilonr_GaAs)*1E9

function Vd = Vd_fun(VM,X,NC)
VT = 0.026;
ND = 1E16*1E6;
Vd = VM - X - VT * log(NC/ND);
end

function ds = ds_fun(Vd,epsilonr)
epsilon0 = 8.85E-12;
VA = 0;
q = 1.6E-19;
ND = 1E16*1E6;
ds = (2*epsilon0*epsilonr/q*(Vd-VA)/ND)^(1/2);
end








