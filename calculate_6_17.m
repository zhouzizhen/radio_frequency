clear
close all

VT = 0.026;
ni = 1.45*10^10*10^6;
NDE = 10^21*10^6;
NAB = 2*10^17*10^6;
NDC = 10^19*10^6;
Vdiff = VT*log(NAB*NDC/(ni^2))

VBE = 0.75;
VCE = 2;
VBC = VBE-VCE;
q = 1.6*10^(-19);
epsilon0 = 8.85*10^(-12);
epsilonr_Si = 11.9;
ds = (2*epsilon0*epsilonr_Si*(Vdiff-VBC)/q * (1/NAB+1/NDC))^(1/2)

A = 10^(-4)*10^(-4);
dE = 0.8*10^(-6);
dB = 1.2*10^(-6);
dC = 2*10^(-6);
mun = 1350*10^(-4);
mup = 480*10^(-4);
IFB = q*mup*(ni^2)*VT / (dE*NDE) * A * (exp(VBE/VT)-1)
IFC = q*mun*(ni^2)*VT / (dB*NAB) * A * (exp(VBE/VT))
IFE = -(IFB+IFC)

betaF = IFC/IFB
betaR = mun*NDC*dC/(mup*NAB*dB)
