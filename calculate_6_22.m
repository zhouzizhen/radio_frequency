clear
close all

q = 1.6*10^(-19);
epsilon0 = 8.85*10^(-12);
epsilonr = 11.7;
d = 2*10^(-6);
WL = 10;
VGS = -1;

mun = 1000*10^(-4);
ND = 1.2*10^17*10^6;

Vp = q*ND*d^2/(2*epsilon0*epsilonr);
VT0 = -3;
Vd = VT0+Vp;
G0 = q^2*mun*ND^2*WL*d;
IDsat = G0*(1/3*Vp - (Vd-VGS) + 2/3/Vp^(1/2)*(Vd-VGS)^(3/2))
VGS = 0;
IDSS = G0*(1/3*Vp - (Vd-VGS) + 2/3/Vp^(1/2)*(Vd-VGS)^(3/2));
VGS = -1;
% 使用平方律近似的饱和电流
Id_sat_square=IDSS*(1-VGS/VT0).^2.*(1-(VGS<VT0))
