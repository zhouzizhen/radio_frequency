function[fig_num,networks]=get_matching(ZS,ZL,f,Z0_in)
% 找出所有可能的两个元件匹配网络
% 计算匹配网络的实际拓扑
% 计算电路元件的值
% 在SmithChart中绘制阻抗变换
% 并且plot出相应的阻抗转换在Smith图中

% f匹配要求的频率
% fig_num Smith图个数
% networks匹配网络描述

% 网络描述
global rf_Network;
global Z0;
Z0=Z0_in;
RL=real(ZL);
XL=imag(ZL);
RS=real(ZS);
XS=imag(ZS);
% 网络计数器
N=0;

syms RL_sym RS_sym X1_sym X2_sym XL_sym XS_sym;
syms ZS_sym ZL_sym
vars = [X1_sym,X2_sym];

%% 源阻抗ZS→并联电抗性元件X1→串联电抗性元件X2→负载阻抗ZL
% 元件电抗
% 用最后的结果的方法
% X1(1)=(-RL*XS+sqrt(RL*RS*(RS^2+XS^2-RL*RS)))/(RL-RS);
% X1(2)=(-RL*XS-sqrt(RL*RS*(RS^2+XS^2-RL*RS)))/(RL-RS);
% X2(1)=-XL+sqrt(-RL^2+RL*RS+RL/RS*XS^2);
% X2(2)=-XL-sqrt(-RL^2+RL*RS+RL/RS*XS^2);

% 用实部和虚部=0的方法
eqns = [ RL_sym*RS_sym+(XL_sym+X2_sym)*(X1_sym+XS_sym)+X1_sym*XS_sym==0,...
    X1_sym*RS_sym-RL_sym*(X1_sym+XS_sym)+(X2_sym+XL_sym)*RS_sym ==0];
output = solve(eqns,vars);
X1 = double(subs(output.X1_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));
X2 = double(subs(output.X2_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));

% 用最初的方程的方法 失败？？？？？？？？？
% eqns = [ 1/(1/(ZS_sym)+1/(1i*X1_sym))+1i*X2_sym==conj(ZL_sym),...
%     ZS_sym == RS_sym+1i*XS_sym,...
%     ZL_sym == RL_sym+1i*XL_sym];
% output = solve(eqns,vars);
% X1 = double(subs(output.X1_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));
% X2 = double(subs(output.X2_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));

%% 计算相应的电抗性元件值
% 如果 X1(1) 和 X2(1) 都是实数
if(imag(X1(1))==0&&imag(X2(1))==0)
    for m=1:2
        N=N+1;
        fig_num(N)=smith_chart;
        init_network;
        Add_shunt_impedance(ZS);
        fprintf('网络%d\n',N);
        fprintf('source->');
        fprintf('shunt ');
        if(X1(m)>=0)
            L1=X1(m)/(2*pi*f);
            fprintf('inductor(%.2eH)->',L1);
            Add_shunt_inductor(L1);
        else
            C1=-1/(2*pi*f)/X1(m);
            fprintf('capacitor(%.2eF)->',C1);
            Add_shunt_capacitor(C1);
        end
        fprintf('series ');
        if(X2(m)>=0)
            L2=X2(m)/(2*pi*f);
            fprintf('inductor(%.2eH)->',L2);
            Add_series_inductor(L2);
        else
            C2=-1/(2*pi*f)/X2(m);
            fprintf('capacitor(%.2eF)->',C2);
            Add_series_capacitor(C2);
        end
        fprintf('load\n');
        rf_imp_transform(f,fig_num(N));
        networks(N,:,:)=rf_Network;
    end
end

%% 源阻抗ZS→串联电抗性元件X1→并联电抗性元件X2→负载阻抗ZL
% 元件电抗
% 用最后的结果的方法
% X1(1)=-XS+sqrt(-RS^2+RL*RS+RS/RL*XL^2);
% X1(2)=-XS-sqrt(-RS^2+RL*RS+RS/RL*XL^2);
% X2(1)=(-RS*XL+sqrt(RL*RS*(RL^2+XL^2-RL*RS)))/(RS-RL);
% X2(2)=(-RS*XL-sqrt(RL*RS*(RL^2+XL^2-RL*RS)))/(RS-RL);

% 用实部和虚部=0的方法
eqns = [ RL_sym*RS_sym+XL_sym*(X1_sym+X2_sym+XS_sym)+X2_sym*(X1_sym+XS_sym)==0,...
    -RL_sym*(X1_sym+X2_sym+XS_sym)+(X2_sym+XL_sym)*RS_sym ==0];
output = solve(eqns,vars);
X1 = double(subs(output.X1_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));
X2 = double(subs(output.X2_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));

% 用最初的方程的方法 失败？？？？？？？？？
% eqns = [ 1/(1/(1i*X2_sym)+1/(ZS_sym+1i*X1_sym))==conj(ZL_sym),...
%     ZS_sym == RS_sym+1i*XS_sym,...
%     ZL_sym == RL_sym+1i*XL_sym];
% output = solve(eqns,vars);
% X1 = double(subs(output.X1_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));
% X2 = double(subs(output.X2_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));

%% 计算相应的电抗性元件值
% 如果 X1(1) 和 X2(1) 都是实数
if(imag(X1(1))==0&&imag(X2(1))==0)
    for m=1:2
        N=N+1;
        fig_num(N)=smith_chart;
        init_network,
        Add_shunt_impedance(ZS);
        fprintf('网络%d\n',N);
        fprintf('source→');
        fprintf('shunt ');
        if(X1(m)>=0)
            L1=X1(m)/(2*pi*f);
            fprintf('inductor(%.2eH)->',L1);
            Add_series_inductor(L1);
        else
            C1=-1/(2*pi*f)/X1(m);
            fprintf('capacitor(%.2eF)->',C1);
            Add_series_capacitor(C1);
        end
        fprintf('series ');
        if(X2(m)>=0)
            L2=X2(m)/(2*pi*f);
            fprintf('inductor(%.2eH)->',L2);
            Add_shunt_inductor(L2);
        else
            C2=-1/(2*pi*f)/X2(m);
            fprintf('capacitor(%.2eF)->',C2');
            Add_shunt_capacitor(C2);
        end
        fprintf('load\n');
        rf_imp_transform(f,fig_num(N));
        networks(N,:,:)=rf_Network;
    end
end
end
