clear
close all

f = (30:1:3000).*10^6;
L = 10E-9;
C = 10E-12;
R = 5;
XC = 1./(1i*2*pi*f*C);
XL = (1i*2*pi*f*L);

%% 并联LC电路的阻抗
X = (1./(1./XC+1./XL));
% X = 1i*(2*pi*f)*L./(1-(2*pi*f).^2*L*C);
loglog(f/1e6,abs(X));hold on
X = (1./(1./XC+1./(XL+R)));
loglog(f/1e6,abs(X));
grid on;
legend('理想电感','理想电感和一个5Ω的电阻串联电路')
xlabel('频率f(MHz)')
ylabel('|X|')
title('并联LC电路的阻抗')

%% 串联LC电路的阻抗
figure
X = XC+XL;
loglog(f/1e6,abs(X));hold on
X = XC+XL+R;
loglog(f/1e6,abs(X));
grid on;
legend('理想电感','理想电感和一个5Ω的电阻串联电路')
xlabel('频率f(MHz)')
ylabel('|X|')
title('串联LC电路的阻抗')


