clear
close all

L = 10E-9;
C = 1E-12;
R=(0:1:200);
fr = (1/(2*pi)).*sqrt((L-R.^2*C)./(L^2*C));
plot(R,fr/1e6);
grid on;
xlabel('����R(��)')
ylabel('г��Ƶ��fr(MHz)')