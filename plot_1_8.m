clear
close all

f = 1e6:1e6:5*1e9;
L = 10*1E-9;
C = 1E-12;
R = 50;
V = (1./(1i*2*pi*f*C+1./(1i*2*pi*f*L)))./...
    (R+1./(1i*2*pi*f*C+1./(1i*2*pi*f*L)));
plot(f/1e9,abs(V));grid on;
xlabel('Ƶ��f(GHz)')
ylabel('|V0/Vi|')