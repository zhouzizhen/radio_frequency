clear
close all

guiyihua = @(tp,w) -20.*log(abs(1+1i*w*tp));
f=(0:1:20)*10^9;
w=2*pi*f;
% w=f;
% w=(0:1:20)*10^9;
tp1 = 10*10^(-12);
plot(w,guiyihua(tp1,w));hold on;
tp2 = 1*10^(-9);
plot(w,guiyihua(tp2,w));hold on;
tp3 = 1*10^(-6);
plot(w,guiyihua(tp3,w));hold on;
grid on;
legend(['tp=',num2str(tp1),'s'],...
    ['tp=',num2str(tp2),'s'],...
    ['tp=',num2str(tp3),'s'])
xlabel('角频率w(rad/s)')
ylabel('归一化电荷响应')