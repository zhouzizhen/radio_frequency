clear
close all

Wg0 = 1.16;
alphaT = 7.02*10^(-4);
betaT = 1108;
T = 200:450;
WgT = @(T) Wg0-alphaT.*T.^2./(betaT+T);
q = 1.6*10^(-19);
k = 1.38066*10^(-23);
VT = @(T) k/q*T;
pt = 3;
IsT0 = 5*10^(-15);
T0 = 300;
n = 1.2;
Is = @(T) IsT0*(T/T0).^(pt/n).*exp(-WgT(T)./VT(T).*(1-T/T0));
VA = 0.7;
IQ = @(T) Is(T).*(exp(VA./(n.*VT(T)))-1);
Rd = @(T) n.*VT(T)./(IQ(T)+Is(T));
VQ = VA;
taoT = 100*10^(-12);
Cd = @(T) Is(T).*taoT./(n.*VT(T)).*exp(VQ./(n*VT(T)));

figure
semilogy(T,Rd(T));
grid on;
xlabel('温度T(K)')
ylabel('微分电阻Rd(Ω)')
figure
semilogy(T,Cd(T)*1E12);
grid on;
xlabel('温度T(K)')
ylabel('微分电容Cd(pF)')






