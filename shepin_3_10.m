% Problem 3.10
clear
close all

Z0 = 50;
ZL = 25-1i*30;
gamma_0 = (ZL - Z0)/(ZL + Z0);
disp(['负载反射系数 : gamma_ 0 = ' num2str(gamma_0)]);
[th0,mag_gamma_0] = cart2pol(real(gamma_0), imag (gamma_0));
fprintf('gamma_0 的极坐标 \nmag_gamma_0 = %f\nangle = %f deg \n', mag_gamma_0, th0*180/pi);

%% 
if th0<0
    th0 = th0 + 2*pi;
end
d = 0.15;
th_in = th0 - 2*2*pi*d;
if th_in< 0
    th_in = th_in + 2*pi;
end
[x_gamma_in, y_gamma_in] = pol2cart(th_in, mag_gamma_0) ;
gamma_in = (x_gamma_in+1i*y_gamma_in);
Zin = Z0*(1+gamma_in) / (1-gamma_in) ;
disp(['输入阻抗 : Zin = ',num2str(Zin)]) ;

%% 
SWR = (1+abs(gamma_0))/(1-abs(gamma_0)) ;
fprintf('SWR = %f \n' , SWR) ;

%% 
smithplot();hold on;
th = th0: (th_in - th0)/29:th_in;
gamma = mag_gamma_0*ones(1,30);
polar(th,gamma);
text(-0.4,0.36,'Z_{in}');
text(-0.12,-0.5, 'Z_L');
