%Problem 3.18a
clear
close all;

R=30;
L=10*1e-9;
C=2.5*10*1e-12;
Z0=75;
vp=3*1e8;

%% 3个小问3种频率
% f=0.1*1e9;
% % 最优长度初始值
% l0=0.85;
% % 长度绘图范围
% l=0:0.0001:0.9;

% f=0.5*1e9;
% % 最优长度初始值
% l0=0.07;
% % 长度绘图范围
% l=0:0.0001:0.08;

f=2*1e9;
% 最优长度初始值
l0=0.005;
% 长度绘图范围
l=0:0.0001:0.006;

%% 
ZL=R+1i*(2*pi*f*L-1/(2*pi*f*C));
gamma_0=(ZL-Z0)./(ZL+Z0);
SWR=(1+abs(gamma_0))./(1-abs(gamma_0));
fprintf('SWR = %f',SWR);
fprintf('\n');

%% 
beta=2*pi*f/vp;

Zin=Z0*(ZL+1i*Z0*tan(beta*l))./(Z0+1i*ZL*tan(beta*l));

%% 
plot(l,real(Zin));hold on;
plot(l,Z0*ones(1,length(l)));
grid on;
xlabel('长度m')
ylabel('Re(Zin) \Omega');

%% 
syms l
assume(l,'real')
eqn = real(Z0*(ZL+1i*Z0*tan(beta*l))./(Z0+1i*ZL*tan(beta*l))) == Z0;
lopt = vpasolve(eqn,l,l0);
fprintf('最优长度lopt = %fm\n',lopt);

Zin=Z0*(ZL+1i*Z0*tan(beta*lopt))./(Z0+1i*ZL*tan(beta*lopt));
Zin=double(Zin);
fprintf('Zin(lopt) = (%f+j(%f))Ohm',real(Zin),imag(Zin));
fprintf('\n');

%% 
Ccompensation = 1/(2*pi*f*imag(Zin));
fprintf('补偿电容 C = %fpF',Ccompensation*10^12);
fprintf('\n');



