% 《射频电路设计-理论与应用》3.25
clc
clear

Z0=50; % generator impedance
Pin=3;
VG=25; % voltage amplitude
ZLmatch=50;
Gamma0=(ZLmatch-Z0)/(ZLmatch+Z0);
GammaS=1-(8*Z0*Pin)^(1/2)/VG;
RG=Z0*(1+GammaS)/(1-GammaS)

Z0_=(RG*Z0)^(1/2);
GammaS_=(RG-Z0_)/(RG+Z0_);
ZL=75+1i*20;
Gamma0=(ZL-Z0)/(ZL+Z0);
l=1/3;
% ZLnew
ZLnew=Z0*(1+Gamma0*exp(-1i*2*2*pi*l))/...
    (1-Gamma0*exp(-1i*2*2*pi*l))
GammaL_=(ZLnew-Z0_)/(ZLnew+Z0_);
l=1/4;
PL=VG^2/(8*Z0_)*abs(1-GammaS_)^2*...
    (1-abs(GammaL_*exp(-2*1i*2*pi*l))^2)/...
    abs(1-GammaS_*GammaL_*exp(-2*1i*2*pi*l))^2
