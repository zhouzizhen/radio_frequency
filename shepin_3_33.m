%Problem 3.33
clear
f=950*1e6;
L=4*1e-9;
C=2*1e-12;
R2=30;
Z0=50;

R1=75;
XL=1i*2*pi*f*L;
ZL=1/(1/R1+1/(XL));
disp(['ZL(Ohm)=',num2str(ZL)]);

Zin1=Z0*(ZL+1i*Z0*tan(2*pi*0.1))/(Z0+1i*ZL*tan(2*pi*0.1));
Zin2=1/(1/R2+1/Zin1);
disp(['Zin2(Ohm)=',num2str(Zin2)]);

Zin3=Z0*(Zin2+1i*Z0*tan(2*pi*0.25))/(Z0+1i*Zin2*tan(2*pi*0.25));
Zshunt=1i*Z0*tan(2*pi*0.1);
Zin4=1/(1/Zin3+1/Zshunt);
disp(['Zin4(Ohm)=',num2str(Zin4)]);

Zin5=Z0*(Zin4+1i*Z0*tan(2*pi*0.25))/(Z0+1i*Zin4*tan(2*pi*0.25));
Zin=Zin5+1/(1i*2*pi*f*C);
disp(['Zin(Ohm)=',num2str(Zin)]);


disp(['|Zin|(Ohm)=',num2str(abs(Zin))]);
disp(['theta(��)=',num2str(angle(Zin)*180/(2*pi))]);
