clear

ZL=20+1i*40;
Z0=50;
betal1=pi*164.3/180;
betal2=pi*57.7/180;
betal3=pi*25.5/180;

%% Problem 3.35
disp('Problem 3.35(a)');
Zin1=Z0*(ZL+1i*Z0*tan(2*betal1))/(Z0+1i*ZL*tan(2*betal1));
disp(['Zin1(Ohm)=' num2str(Zin1)]);

Zshunt=1i*Z0*tan(2*betal2);
Zin2=1/(1/Zin1+1/Zshunt);
disp(['Zin2(Ohm)=' num2str(Zin2)]);
Zin=Z0*(Zin2+1i*Z0*tan(2*betal3))/(Z0+1i*Zin2*tan(2*betal3));
disp(['Zin(Ohm)=' num2str(Zin)]);

%% Problem 3.35(b)
disp('Problem 3.35(b)');
Zin1=Z0*(ZL+1i*Z0*tan(2*betal1))/(Z0+1i*ZL*tan(2*betal1));
disp(['Zin1(Ohm)=' num2str(Zin1)]);

Zshunt=-1i*Z0*1/tan(2*betal2);
Zin2=1/(1/Zin1+1/Zshunt);
disp(['Zin2(Ohm)=' num2str(Zin2)]);
Zin=Z0*(Zin2+1i*Z0*tan(2*betal3))/(Z0+1i*Zin2*tan(2*betal3));
disp(['Zin(Ohm)=' num2str(Zin)]);
