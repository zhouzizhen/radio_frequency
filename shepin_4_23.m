clear
close all

L = (0:100).*1E-9;
f = 2*10^9;
Z0 = 50;
R1 = 8.65;
R2 = 8.65;
R3 = 141.8;
Zin = R1+(R2+Z0).*(R3+1i*2*pi*f*L)./(R2+Z0+R3+1i*2*pi*f*L);
Gamma_in = (Zin-Z0)./(Zin+Z0);
IL = -10*log10(1-abs(Gamma_in).^2);

figure
plot(L/1E-9,IL);
grid on;
xlabel('���L(nH)')
ylabel('�������IL(dB)')

Zout = R2+(R1+Z0).*(R3+1i*2*pi*f*L)./(R1+Z0+R3+1i*2*pi*f*L);
S = [(Zin-Z0)./(Zin+Z0),2*(Zin-R1)./(Zin+Z0)*Z0/(R2+Z0);
    2*(Zin-R1)./(Zin+Z0)*Z0/(R2+Z0),(Zout-Z0)./(Zout+Z0)];
