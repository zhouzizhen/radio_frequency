clear
close all

f = 2*10^9;
Z0 = 50;
R1 = 8.65;
R2 = 8.65;
R3 = 141.8;
Zin = @(L) R1+(R2+Z0).*(R3+1i*2*pi*f*L)./(R2+Z0+R3+1i*2*pi*f*L);
Gamma_in = @(L) (Zin(L)-Z0)./(Zin(L)+Z0);
IL = @(L) -10*log10(1-abs(Gamma_in(L)).^2)

L = (0:100).*1E-9;

figure
plot(L/1E-9,IL(L));
grid on;
xlabel('���L(nH)')
ylabel('�������IL(dB)')
