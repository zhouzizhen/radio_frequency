clear;
close all

fL = 1.9e9;  
fU = 2.0e9;
fc = 1.95e9;
wU = 2*pi*fU;
wL = 2*pi*fL;
wc = 2*pi*fc;
wBW = (wU-wL);

f0 = (fL+fU)/2;
% 由(5.52)可知，代入f=2.1GHz可得归一化频率
f = 2.1e9;
omega_guiyihua = f0/(fU-fL)*(f/f0-f0/f)
% 由(5.52)可知，代入f=1.8GHz可得归一化频率
f = 1.8e9;
omega_guiyihua = f0/(fU-fL)*(f/f0-f0/f)

%% 由表5.4(b)可知，N=3时，元件参数为
g0 = 1; 
g1 = 1.5963;
g2 = 1.0967; 
g3 = 1.5963;
g4 = 1;

%% 图5.17第2种等效 计算实际元件值
% 由(5.56)可知，反归一化串联电感
L1 = g1/wBW;
% 由(5.56)可知，反归一化串联电容
C1 = wBW/(g1*wc*wc);
% 由(5.58)可知，反归一化并联电感
L2 = wBW/(g2*wc*wc);
% 由(5.58)可知，反归一化并联电容
C2 = g2/wBW;
% 由(5.56)可知，反归一化串联电感
L3 = g3/wBW;
% 由(5.56)可知，反归一化串联电容
C3 = wBW/(g3*wc*wc);

%% 计算频率响应
freq = linspace(0,3*fc,3000);
H = zeros(1,length(freq));
for i=1:length(freq)
    omega = 2*pi*freq(i);
    A0 = [1 1/g0;0 1];
    A1 = [1 1i*omega*L1 + 1./(1i*omega*C1);0 1];
    A2 = [1 0;1i*omega*C2 + 1./(1i*omega*L2) 1];
    A3 = [1 1i*omega*L3 + 1./(1i*omega*C3);0 1];
    A4 = [1 0;g4 1];
    A = A0*A1*A2*A3*A4;
    H(i) = 2./(A(1));
end

figure(1);
plot(freq/1e9,-20*log10(abs(H)));hold on
scatter(1.9,0.5,400,'.');hold on
scatter(2,0.5,400,'.');hold on
scatter(1.8,30,400,'.');hold on
scatter(2.1,30,400,'.');hold on
xlabel('频率 Ghz'); 
ylabel('衰减 dB');
grid on;
ylim([0 40]);
title('3阶带通滤波器');
