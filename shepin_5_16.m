clear 
close all;
%
fL = 2.5*1e9;  
fU = 3.6*1e9;
fc = sqrt(fU*fL);
fprintf('fc=%fGHz\n',fc/1e9);

f = fU;
% 由(5.52)可知，代入f可得归一化频率，取倒数可得带阻滤波器的
omega=(fc/(fU-fL)*(f./fc-fc./f))^(-1);
fprintf('Omegs(fc)=%f\n',omega);

syms fL_30 fU_30 
[fL_30,fU_30]=solve(fc == sqrt(fU_30*fL_30),fc*0.1 == (fU_30-fL_30),[fL_30,fU_30]);
fL_30 = double(fL_30);
fU_30 = double(fU_30);
fL_30 = fL_30(find(fL_30>0))
fU_30 = fU_30(find(fU_30>0))

%% 由3dB波纹的表5.4(a)可知，N=4时的元件参数为
g0 = 1; 
g1 = 3.4389;
g2 = 0.7483; 
g3 = 4.3471;
g4 = 0.5920;
g5 = 5.8095;

%% 图5.17第2种等效 计算实际元件值
wsqr = 2*pi*fc*2*pi*fc;
BW   = 2*pi*fU-2*pi*fL;

L1 = g1*BW/wsqr;
C1 = 1/(BW*g1);
L2 = 1/(BW*g2);
C2 = g2*BW/wsqr;
L3 = g3*BW/wsqr;
C3 = 1/(BW*g3);
L4 = 1/(BW*g4);
C4 = g4*BW/wsqr;

%% 计算频率响应
f = 0;
for i=1:2000
    omega = 2*pi*f;
    A0 = [1 g0;0 1];
    A1 = [1 1./(1i*omega*C1-1i./(omega*L1));0 1];
    A2 = [1 0;1./(1i*omega*L2-1i./(omega*C2)) 1];
    A3 = [1 1./(1i*omega*C3-1i./(omega*L3));0 1];
    A4 = [1 0;1./(1i*omega*L4-1i./(omega*C4)) 1];
    A5 = [1 0;1/g5 1];
    A  = A0*A1*A2*A3*A4*A5;
    H(i) = 1/(1+g0/g5)./A(1);
    freq(i) = f;
    f = f+0.001*fc;
end
figure(1);
plot(freq/1e9,-20*log10(abs(H)),'LineWidth',2.0);hold on
scatter(fL/1e9,3,400,'.');hold on
scatter(fU/1e9,3,400,'.');hold on
scatter(fL_30/1e9,30,400,'.');hold on
scatter(fU_30/1e9,30,400,'.');hold on
grid on;
ylim([-1 40]);
% xlim([0 6]);
xlabel('频率 Ghz'); 
ylabel('衰减 dB');
title('4阶带阻滤波器');


