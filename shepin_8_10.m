% problem 8.10
close all
clear
clc
addpath(genpath('E:\zzz\learning\��Ƶ\RF_matlab'))

% �������迹
% Set_Z0(50); 
% �����迹
ZL=100;
f0=600e6;

%% 
sm=smith_chart;
% plot Qn=4 ����
Qn_contours(4,sm);
text(-0.24,0.86,'\bfQ_n=4');
text(-0.24,-0.86,'\bfQ_n=4');

%��������
global rf_Network;
% ��ʼ����������
init_network; 

Add_shunt_impedance(ZL);
text(0.29, -0.09,'\bfz_L');

L1=42e-9;
Add_series_inductor(L1);
text(0.66, 0.42,'\bfA');

C2=4.3e-12;
Add_shunt_capacitor(C2);
text(0.38, -0.78,'\bfB');

L3=11e-9;
Add_series_inductor(L3);
text(-0.06, -0.51,'\bfz_{in}');

Zin=rf_imp_transform(f0,sm)

clear

ZL=100;
f0=600e6;
%% 
sm=smith_chart;
% plot Qn=4 ����
Qn_contours(4,sm);
text(-0.24,0.86,'\bfQ_n=4');
text(-0.24,-0.86,'\bfQ_n=4');

%��������
global rf_Network;
% ��ʼ����������
init_network; 

Add_shunt_impedance(ZL);
text(0.23, -0.09,'\bfz_L');

C1=1.7e-12;
Add_series_capacitor(C1);
text(0.64, -0.23,'\bfA');

C2=1.9e-12;
Add_shunt_capacitor(C2);
text(0.36, -0.77,'\bfB');

L3=10.7e-9;
Add_series_inductor(L3);
text(-0.06, -0.51,'\bfz_{in}');

Zin=rf_imp_transform(f0,sm)

