% Problem 8.12

close all;
clear 

addpath(genpath('E:\zzz\learning\��Ƶ\RF_matlab'))

% �������迹
Set_Z0(50); 
% �����迹
ZL=10;
f0=500e6;

sm=smith_chart;
% plot Qn=1 ����
Qn_contours(1,sm);
%��������
global rf_Network;
L1=3.2e-9;  C1=15.9e-12;
L2=6.3e-9;  C2=8e-12;
L3=12.5e-9; C3=4e-12;
L4=25e-9;   C4=2.1e-12;
L5=40e-9;   C5=0.98e-12;
%define a matching
init_network; % ��ʼ����������
Add_shunt_impedance(ZL);
Add_series_inductor(L1);
Add_shunt_capacitor(C1);
Add_series_inductor(L2);
Add_shunt_capacitor(C2);
Add_series_inductor(L3);
Add_shunt_capacitor(C3);
Add_series_inductor(L4);
Add_shunt_capacitor(C4);
Add_series_inductor(L5);
Add_shunt_capacitor(C5);
Zin=rf_imp_transform(f0,sm)
text(-0.65, -0.08,'\bfz_L');
text(-0.64, 0.36,'\bfA');
text(-0.44, -0.08,'\bfB');
text(0.66, -0.08,'\bfz_S');

