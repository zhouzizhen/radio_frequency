% Problem 8.15

close all;
clear

global Z0;
% 特性线阻抗                                 
Set_Z0(75); 
% 负载阻抗
ZL=80+1i*20;
Zin=30-1i*10;

f0=1e9;

%% 
%create Smith chart
sm=smith_chart;
const_SWR_circle(Zin,'m--');
const_G_circle(real(1/ZL),'m--');

% 定义匹配网路
% open-circuit stub
I_S=0.134;
I_L=0.075;
% 初始化网络描述
init_network; 
Add_shunt_impedance(ZL);
Add_shunt_oc_stub(360*I_S,f0,Z0);
Add_trans_line(360*I_L,f0,Z0);
Zin=rf_imp_transform(f0,sm)

text(0.04, 0.22,'\bfz_L');
text(-0.15, -0.48,'\bfA');
text(-0.55, -0.11,'\bfz_{in}');


%% 两种匹配网络实现的输入阻抗频率响应的比较
figure;
% fc 重新定义匹配网络以考虑负载电感
LL=imag(ZL/2/pi/f0);
RL=real(ZL);
% 频率范围
f=(0.2:0.01:1.6)*f0;
length_f=length(f);

%% 开路短线匹配网络实现的输入阻抗频率响应的比较
I_S=0.134;
I_L=0.075;
init_network;
Add_shunt_impedance(RL);
Add_series_inductor(LL);
Add_shunt_oc_stub(360*I_S,f0,Z0);
Add_trans_line(360*I_L,f0,Z0);
% 输入阻抗
Zin=zeros([1 length_f]);
for n=1:length_f
    Zin(n)=rf_get_impedance(f(n));
end
subplot(2,1,1)
plot(f/1e9,real(Zin),'b');
hold on;
subplot(2,1,2)
plot(f/1e9,imag(Zin),'b');
hold on;

%% 短路短线匹配网络实现的输入阻抗频率响应的比较
% 定义匹配网络 with short-circle stub
I_S=0.134+0.25;%short-circuit stub
I_L=0.075;
% 初始化网络描述
init_network; 
Add_shunt_impedance(RL);
Add_series_inductor(LL);
Add_shunt_sc_stub(360*I_S,f0,Z0);
Add_trans_line(360*I_L,f0,Z0);
% 输入阻抗
for n=1:length_f
    Zin(n)=rf_get_impedance(f(n));
end
subplot(2,1,1)
plot(f/1e9,real(Zin),'r-.');
hold on;
grid on;
xlabel('频率 f,GHz');
ylabel('输入电阻 R_{in},\Omega');
title('匹配网络的输入阻抗');
legend('open-circuit stub 开路短线','short-circuit stub 短路短线');

subplot(2,1,2)
plot(f/1e9,imag(Zin),'r-.');
hold on;
grid on;
xlabel('频率 f,GHz');
ylabel('输入电抗 X_{in},\Omega');
legend('open-circuit stub 开路短线','short-circuit stub 短路短线');

