% Problem 8.17

close all;
clear

global Z0;
% �������迹
Set_Z0(50);
% �����迹
ZL=20-1i*20;

% create Smith chart
sm=smith_chart;

% forbidden region Բ(g=2)
const_G_circle(real(2/Z0),'m');
text(-0.9,	-0.05,  'Forbidden');
text(-0.9,	-0.15,  'region');
% g=1 Բ
const_G_circle(real(1/Z0),'c--');

global rf_Network;%

% ��������� f=1GHz
f=1e9;
% ��ʼ����������
init_network;

Add_shunt_impedance(ZL);
text(-0.33,	-0.46,  '\bfy_L');

l1=1/8;
Add_trans_line(360*l1,f,Z0);
text(-0.42,	0.40,   '\bfy_D');

% ��ת g=1 Բ
a=(0:360)*pi/180;
hold on;
plot(0.5*cos(a),-0.5+0.5*sin(a),'c--');

ls1=0.4511;
Add_shunt_sc_stub(360*ls1,f,Z0);
text(-0.58,	-0.40,  '\bfy_C');

l2=5/8;
Add_trans_line(360*l2,f,Z0);
text(-0.42,	0.59,   '\bfy_B');

ls2=0.4087;
Add_shunt_sc_stub(360*ls2,f,Z0);
text(0.03,	-0.05,  '\bfy_A');

l3=3/8;
Add_trans_line(360*l3,f,Z0);
Zin=rf_imp_transform(f,sm)

