clear
close all;
clc

theta0 = 270/180*pi;
% 由(8.28)可知，放大器的最大效率
n = (theta0-sin(theta0))/(2*(theta0*cos(theta0/2)-2*sin(theta0/2)));