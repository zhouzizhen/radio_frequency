clear
close all;
clc

addpath(genpath('E:\zzz\learning\射频\RF_matlab'))

ZS=10+1i*25;
ZL=100+1i*20;
Z0=50;
f0=960e6;
% get_matching(ZS,ZL,f0,Z0);

% 网络描述
global rf_Network;
% global Z0;
% Z0=Z0_in;
RL=real(ZL);
XL=imag(ZL);
RS=real(ZS);
XS=imag(ZS);
% 网络计数器
N=0;

syms RL_sym RS_sym X1_sym X2_sym XL_sym XS_sym;
syms ZS_sym ZL_sym;
vars = [X1_sym,X2_sym];

%% 源阻抗ZS→并联电抗性元件X1→串联电抗性元件X2→负载阻抗ZL
% 元件电抗
% X1(1)=(-RL*XS+sqrt(RL*RS*(RS^2+XS^2-RL*RS)))/(RL-RS);
% X1(2)=(-RL*XS-sqrt(RL*RS*(RS^2+XS^2-RL*RS)))/(RL-RS);
% X2(1)=-XL+sqrt(-RL^2+RL*RS+RL/RS*XS^2);
% X2(2)=-XL-sqrt(-RL^2+RL*RS+RL/RS*XS^2);

% eqns = [ RL_sym*RS_sym+(XL_sym+X2_sym)*(X1_sym+XS_sym)+X1_sym*XS_sym==0,...
%     X1_sym*RS_sym-RL_sym*(X1_sym+XS_sym)+(X2_sym+XL_sym)*RS_sym ==0];
% output = solve(eqns,vars);
% X1 = double(subs(output.X1_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));
% X2 = double(subs(output.X2_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));

% eqns = [ 1/(1/(ZS_sym)+1/(1i*X1_sym))+1i*X2_sym==conj(ZL_sym),...
%     ZS_sym == RS_sym+1i*XS_sym,...
%     ZL_sym == RL_sym+1i*XL_sym];
% output = solve(eqns,vars);
% X1 = double(subs(output.X1_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));
% X2 = double(subs(output.X2_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));

eqns = [ 1/(1/(1i*X2_sym)+1/(ZS_sym+1i*X1_sym))==conj(ZL_sym),...
    ZS_sym == RS_sym+1i*XS_sym,...
    ZL_sym == RL_sym+1i*XL_sym];
output = solve(eqns,vars);
X1 = double(subs(output.X1_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));
X2 = double(subs(output.X2_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[RS,RL,XS,XL]));

