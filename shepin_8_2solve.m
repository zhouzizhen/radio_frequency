clear
close all;
clc

f=960e6;
N=0;

%% 源阻抗ZS→并联电抗性元件X1→串联电抗性元件X2→负载阻抗ZL
syms RL_sym RS_sym X1_sym X2_sym XL_sym XS_sym;
syms ZS_sym ZL_sym;
% eqns = [ RL_sym*RS_sym+(XL_sym+X2_sym)*(X1_sym+XS_sym)+X1_sym*XS_sym==0,...
%     X1_sym*RS_sym-RL_sym*(X1_sym+XS_sym)+(X2_sym+XL_sym)*RS_sym ==0];
% eqns = [ 1/(1/(ZS_sym)+1/(1i*X1_sym))+1i*X2_sym==conj(ZL_sym),...
%     ZS_sym == (RS_sym+1i*XS_sym),...
%     ZL_sym == (RL_sym+1i*XL_sym)];
% eqns = [ 1/(1/((RS_sym+1i*XS_sym))+1/(1i*X1_sym))+1i*X2_sym==conj((RL_sym+1i*XL_sym))];
eqns = 1/(1/(RS_sym+1i*XS_sym)+1/(1i*X1_sym))+1i*X2_sym==(RL_sym-1i*XL_sym);
vars = [X1_sym,X2_sym];
output = solve(eqns,vars);
X1 = double(subs(output.X1_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[10,100,25,20]));
X2 = double(subs(output.X2_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[10,100,25,20]));

% 如果 X1(1) 和 X2(1) 都是实数
if(imag(X1(1))==0&&imag(X2(1))==0)
    for m=1:2
        N=N+1;
        fprintf('网络%d\n',N);
        fprintf('source->');
        fprintf('shunt ');
        if(X1(m)>=0)
            L1=X1(m)/(2*pi*f);
            fprintf('inductor(%.2eH)->',L1);
        else
            C1=-1/(2*pi*f)/X1(m);
            fprintf('capacitor(%.2eF)->',C1);
        end
        fprintf('series ');
        if(X2(m)>=0)
            L2=X2(m)/(2*pi*f);
            fprintf('inductor(%.2eH)->',L2);
        else
            C2=-1/(2*pi*f)/X2(m);
            fprintf('capacitor(%.2eF)->',C2);
        end
        fprintf('load\n');
    end
end

%% 源阻抗ZS→串联电抗性元件X1→并联电抗性元件X2→负载阻抗ZL
syms  RL_sym RS_sym X1_sym X2_sym XL_sym XS_sym;
% eqns = [ RL_sym*RS_sym+XL_sym*(X1_sym+X2_sym+XS_sym)+X2_sym*(X1_sym+XS_sym)==0,...
%     -RL_sym*(X1_sym+X2_sym+XS_sym)+(X2_sym+XL_sym)*RS_sym ==0];
% eqns = [ 1/(1/(1i*X2_sym)+1/(ZS_sym+1i*X1_sym))==conj(ZL_sym),...
%     ZS_sym == (RS_sym+1i*XS_sym),...
%     ZL_sym == (RL_sym+1i*XL_sym)];
% eqns = 1/(1/(1i*X2_sym)+1/((RS_sym+1i*XS_sym)+1i*X1_sym))==conj(RL_sym+1i*XL_sym);
eqns = 1/(1/(1i*X2_sym)+1/((RS_sym+1i*XS_sym)+1i*X1_sym))== (RL_sym-1i*XL_sym);
vars = [X1_sym,X2_sym];
output = solve(eqns,vars);
X1 = double(subs(output.X1_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[10,100,25,20]));
X2 = double(subs(output.X2_sym,[RS_sym,RL_sym,XS_sym,XL_sym],[10,100,25,20]));

% 如果 X1(1) 和 X2(1) 都是实数
if(imag(X1(1))==0&&imag(X2(1))==0)
    for m=1:2
        N=N+1;
        fprintf('网络%d\n',N);
        fprintf('source→');
        fprintf('shunt ');
        if(X1(m)>=0)
            L1=X1(m)/(2*pi*f);
            fprintf('inductor(%.2eH)->',L1);
        else
            C1=-1/(2*pi*f)/X1(m);
            fprintf('capacitor(%.2eF)->',C1);
        end
        fprintf('series ');
        if(X2(m)>=0)
            L2=X2(m)/(2*pi*f);
            fprintf('inductor(%.2eH)->',L2);
        else
            C2=-1/(2*pi*f)/X2(m);
            fprintf('capacitor(%.2eF)->',C2');
        end
        fprintf('load\n');
    end
end








