clear
close all;
clc

addpath(genpath('E:\zzz\learning\射频\RF_matlab'))

ZS=50;
ZL=20+1i*10;
Z0=50;
f0=800e6;

get_matching(ZS,ZL,f0,Z0);

%% 
% 负载电感
LL=imag(ZL)/(2*pi*f0);
% 负载电阻
RL=real(ZL); 

figure;
% 定义要绘制阻抗的频率范围
f=(0.01:0.01:4)*1e9;
w=2*pi*f; 

%% 查找第一个匹配网络的传递函数
% source->shunt inductor(8.12e-09H)->series capacitor(5.77e-12F)->load
L=8.12e-9;
C=5.77e-12;
% 负载电感阻抗
Z_LL=1i*w*LL;
% 匹配网络组件的阻抗
Z_L=1i*w*L;
Z_C=1./(1i*w*C);
Ztemp=RL+Z_LL+Z_C;
% 网络输入阻抗
Zin=Z_L.*Ztemp./(Z_L+Ztemp);
% 传递函数
H=RL./Ztemp.*Zin./(Zin+ZS);
plot(f/1e9,20*log10(abs(H)),'r');
hold on;

%% 查找第二个匹配网络的传递函数
% source->shunt capacitor(4.87e-12F)->series inductor(2.88e-09H)->load
L=2.88e-9;
C=4.87e-12;
% 负载电感阻抗
Z_LL=1i*w*LL;
% 匹配网络组件的阻抗
Z_L=1i*w*L;
Z_C=1./(1i*w*C);
Ztemp=RL+Z_LL+Z_L;
% 网络输入阻抗
Zin=Z_C.*Ztemp./(Z_C+Ztemp);
% 传递函数
H=RL./Ztemp.*Zin./(Zin+ZS);
plot(f/1e9,20*log10(abs(H)),'b');

%% 
ylim([-15 -10]);
grid on
title('两个匹配网络的传输函数');
xlabel('频率 f GHz');
ylabel('传递函数 |H| dB');
legend('并联电感，串联电容 图8-1(b)','并联电容，串联电感 图8-1(h)');



