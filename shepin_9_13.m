clear
close all
clc

S11 = 0.83*exp(1i*(-132)/180*pi);
S12 = 0.03*exp(1i*(22)/180*pi);
S21 = 4.9*exp(1i*(71)/180*pi);
S22 = 0.36*exp(1i*(-82)/180*pi);
deta = S11*S22-S12*S21;
abs_deta = abs(deta)
k = (1-abs(S11)^2-abs(S22)^2+abs(deta)^2)/(2*abs(S12)*abs(S21))
mu = (1-abs(S11)^2)/(abs(S22-conj(S11)*deta)+abs(S21*S12))

% (1-0.83*0.83-0.36*0.36+abs_deta*abs_deta)/(2*0.03*4.9)

%% 
GSmax = 1/(1-abs(S11)^2)
GLmax = 1/(1-abs(S22)^2)
G0 = abs(S21)^2
GTUmax = GSmax*GLmax*G0

GSmax_dB = 10*log10(GSmax)
GLmax_dB = 10*log10(GLmax)
G0_dB = 10*log10(G0)
GTUmax_dB = 10*log10(GTUmax)
GTUmax_dB = GSmax_dB+GLmax_dB+G0_dB