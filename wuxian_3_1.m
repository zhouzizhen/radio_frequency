clear
close all;

L = 0.2*10^(-6);
C = 300*10^(-12);
f = 500*10^6;

%% 
R = 5;
G = 0;
Z0 = ((R+1i*2*pi*f*L)/(G+1i*2*pi*f*C))^(1/2)
gamma = ((R+1i*2*pi*f*L)*(G+1i*2*pi*f*C))^(1/2)

%% 
R = 5;
G = R/L*C;
Z0 = ((R+1i*2*pi*f*L)/(G+1i*2*pi*f*C))^(1/2)
gamma = ((R+1i*2*pi*f*L)*(G+1i*2*pi*f*C))^(1/2)

%% 
R = 0;
G = 0;
Z0 = ((R+1i*2*pi*f*L)/(G+1i*2*pi*f*C))^(1/2)
gamma = 1i*2*pi*f*(L*C)^(1/2)



