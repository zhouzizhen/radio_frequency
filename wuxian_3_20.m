% 3-20《无线》
clear
close all;

R=30;
L=10*1e-9;
C=2.5*10*1e-12;
Z0=50;
vp=3*1e8;
f=2*1e9;

%% 
ZL=R+1i*(2*pi*f*L-1/(2*pi*f*C));
gamma_0=(ZL-Z0)./(ZL+Z0);
SWR=(1+abs(gamma_0))./(1-abs(gamma_0));
fprintf('SWR = %f',SWR);
fprintf('\n');

%% 
beta=2*pi*f/vp;
l=0:0.0025/100:0.0025;

Zin=Z0*(ZL+1i*Z0*tan(beta*l))./(Z0+1i*ZL*tan(beta*l));

%% 
plot(l,real(Zin));hold on;
plot(l,Z0*ones(1,length(l)));
grid on;
xlabel('长度m')
ylabel('Real(Zin) \Omega');

% figure
% plot(l,imag(Zin));
% grid on;
% xlabel('长度m')
% ylabel('image(Zin) \Omega');

%% 
syms l
eqn = real(Z0*(ZL+1i*Z0*tan(beta*l))./(Z0+1i*ZL*tan(beta*l))) == Z0;
lopt = min(vpa(solve(eqn,l,'Real',true),4));
fprintf('最优长度lopt = %fm\n',lopt);

Zin=Z0*(ZL+1i*Z0*tan(beta*lopt))./(Z0+1i*ZL*tan(beta*lopt));
Zin=double(Zin);
fprintf('Zin(lopt) = (%f+j(%f))Ohm',real(Zin),imag(Zin));
fprintf('\n');

%% 
Ccompensation = 1/(2*pi*f*imag(Zin));
fprintf('补偿电容 C = %fpF',Ccompensation*10^12);
fprintf('\n');

