clear

% 均方根值
Vg=15;
Zg=75;
Z0=75;
ZL=60-1i*40; % 负载阻抗
l=0.7;

%% 
Gamma0=(ZL-Z0)/(ZL+Z0)
% GammaS=(Zg-Z0)/(Zg+Z0);
PL1 = (Vg/2)^2/Z0*(1-abs(Gamma0)^2)

%% 
Zin=Z0*(ZL+1i*Z0*tan(2*pi*l))/...
    (Z0+1i*ZL*tan(2*pi*l))
PL2 = abs(Vg/(Zg+Zin))^2*real(Zin)

%% 
Vin = Vg*Zin/(Zin+Zg);
% VL = Vin*ZL/(ZL+Z0);
% VL = Vin*ZL/Zin;
VL = Vin / exp(1i*2*pi*l) / (1+Gamma0*exp(-2*1i*2*pi*l))
PL3 = abs(VL/ZL)^2*real(ZL)
% VL_ = (PL2/real(ZL))^(1/2)*ZL


