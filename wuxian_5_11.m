% 《无线通信射频电路技术与设计》 5.11
clear
close all;
clc

Z0 = 50;
Y0 = 1/Z0;
YL = 0.4+1i*1.2;
GL = real(YL);
BL = imag(YL);
d = 1/8;
t = tan(2*pi*d);
0<=GL && GL<=Y0*(1+t^2)/(t^2)

B1(1) = -BL + (Y0+sqrt(Y0*GL*(1+t^2)-GL*GL*t*t))/t;
B1(2) = -BL + (Y0-sqrt(Y0*GL*(1+t^2)-GL*GL*t*t))/t;

B2(1) = (+Y0*sqrt(Y0*GL*(1+t^2)-GL*GL*t*t)+Y0*GL)/(GL*t);
B2(2) = (-Y0*sqrt(Y0*GL*(1+t^2)-GL*GL*t*t)+Y0*GL)/(GL*t);

lopen1_lambda = atan(B1./Y0)./(2*pi)
% ls_lambda = -atan(Y0./B)./(2*pi)

lopen2_lambda = atan(B2./Y0)./(2*pi)
% ls_lambda = -atan(Y0./B)./(2*pi)

