clear;
close all

fc = 3e9;
fU_fL_fc = 0.1;  
wc = 2*pi*fc;

%% 由表5.4(b)可知，N=3时，元件参数为
g0 = 1; 
g1 = 1.5963;
g2 = 1.0967; 
g3 = 1.5963;
g4 = 1;

%% 图5.17第2种等效 计算实际元件值
Z0 = 75;
% gamma0 无量纲 
% 如果 g0 是电导 那么 gamma0 = Z0*g0;
% 如果 g0 是电阻 那么 gamma0 = Z0/g0;
% 此处认为 g0 是电阻
gamma0 = Z0/g0;

wcwc = 2*pi*fc*2*pi*fc;
BW   = 2*pi*fU_fL_fc*fc;

% 由(5.59)可知，归一化串联电感所对应的反归一化并联电感
L1 = g1*BW/wcwc;
L1 = L1*gamma0;
% 由(5.59)可知，归一化串联电感所对应的反归一化并联电容
C1 = 1/(BW*g1);
C1 = C1/gamma0;

% 由(5.60)可知，归一化并联电容所对应的反归一化串联电感
L2 = 1/(BW*g2);
L2 = L2*gamma0;
% 由(5.60)可知，归一化并联电容所对应的反归一化串联电容
C2 = g2*BW/wcwc;
C2 = C2/gamma0;

% 由(5.59)可知，归一化串联电感所对应的反归一化并联电感
L3 = g3*BW/wcwc;
L3 = L3*gamma0;
% 由(5.59)可知，归一化串联电感所对应的反归一化并联电容
C3 = 1/(BW*g3);
C3 = C3/gamma0;

%% 计算频率响应
freq = linspace(0,2*fc,2000);
H = zeros(1,length(freq));
for i=1:length(freq)
    omega = 2*pi*freq(i);
    % 此处认为 g0 是电阻
    A0 = [1 1/g0;0 1];
    A1 = [1 1./(1i*omega*C1 + 1./(1i*omega*L1));0 1];
    A2 = [1 0;1./(1i*omega*L2 + 1./(1i*omega*C2)) 1];
    A3 = [1 1./(1i*omega*C3 + 1./(1i*omega*L3));0 1];
    A4 = [1 0;g4 1];
    A = A0*A1*A2*A3*A4;
    H(i) = 1./(A(1));
end

figure(1);
plot(freq/1e9,-20*log10(abs(H)));
xlabel('频率 Ghz'); 
ylabel('衰减 dB');
grid on;
ylim([0 50]);
xlim([2 4]);
N = 3;
title([num2str(N),'阶带阻滤波器']);

