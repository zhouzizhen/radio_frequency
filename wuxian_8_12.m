clear;
close all

fc = 200e6;
wc = 2*pi*fc;

% 由(5.46)可知，代入f=500MHz可得归一化频率
f = 500e6;
omega_guiyihua = f/fc

%% 由3dB波纹的表5.4(a)可知，N=5时的元件参数为
g0 = 1; 
g1 = 3.4817;
g2 = 0.7618; 
g3 = 4.5381;
g4 = g2;
g5 = g1;
g6 = 1;

%% 图5.17第2种等效 计算实际元件值
Z0=50;
% 由(5.48)可知，反归一化串联电感
L1 = g1/wc;
% L1 = L1*Z0
% 由(5.48)可知，反归一化并联电容
C2 = g2/wc;
% C2 = C2/Z0
% 由(5.48)可知，反归一化串联电感
L3 = g3/wc;
% L3 = L3*Z0
% 由(5.48)可知，反归一化并联电容
C4 = g4/wc;
% C4 = C4/Z0
% 由(5.48)可知，反归一化串联电感
L5 = g5/wc;
% L5 = L5*Z0

%% 计算频率响应
freq = linspace(0,3*fc,3000);
H = zeros(1,length(freq));
for i=1:length(freq)
    omega = 2*pi*freq(i);
    A0 = [1 1/g0;0 1];
    A1 = [1 1i*omega*L1;0 1];
    A2 = [1 0;1i*omega*C2 1];
    A3 = [1 1i*omega*L3;0 1];
    A4 = [1 0;1i*omega*C4 1];
    A5 = [1 1i*omega*L5;0 1];
    A6 = [1 0;g6 1];
    A = A0*A1*A2*A3*A4*A5*A6;
    H(i) = 2./(A(1));
end

figure(1);
plot(freq/1e6,-20*log10(abs(H)));hold on
scatter(200,3,400,'.');hold on
scatter(500,50,400,'.');hold on
xlabel('频率 Ghz'); 
ylabel('衰减 dB');
grid on;
ylim([0 60]);
N = 5;
title([num2str(N),'阶低通滤波器']);
