clear;
close all

fc = 5e9;
fBW = 150e6;
fL = fc-fBW/2;  
fU = fc+fBW/2;  
wU = 2*pi*fU;
wL = 2*pi*fL;
wc = 2*pi*fc;
wBW = (wU-wL);

% 由(5.52)可知，代入f=2.1GHz可得归一化频率
f = 5.2e9;
omega_guiyihua = fc/(fU-fL)*(f/fc-fc/f)

%% 由0.5dB波纹的表5.4(b)可知，N=4时的元件参数为
g0 = 1; 
g1 = 1.6703;
g2 = 1.1926; 
g3 = 2.3661;
g4 = 0.8419;
g5 = 1.9841;

%% 图5.17第2种等效 计算实际元件值
% 由(5.56)可知，反归一化串联电感
L1 = g1/wBW;
% 由(5.56)可知，反归一化串联电容
C1 = wBW/(g1*wc*wc);
% 由(5.58)可知，反归一化并联电感
L2 = wBW/(g2*wc*wc);
% 由(5.58)可知，反归一化并联电容
C2 = g2/wBW;
% 由(5.56)可知，反归一化串联电感
L3 = g3/wBW;
% 由(5.56)可知，反归一化串联电容
C3 = wBW/(g3*wc*wc);
% 由(5.58)可知，反归一化并联电感
L4 = wBW/(g4*wc*wc);
% 由(5.58)可知，反归一化并联电容
C4 = g4/wBW;

%% 计算频率响应
freq = linspace(0,3*fc,3000);
H = zeros(1,length(freq));
for i=1:length(freq)
    omega = 2*pi*freq(i);
    A0 = [1 1/g0;0 1];
    A1 = [1 1i*omega*L1 + 1./(1i*omega*C1);0 1];
    A2 = [1 0;1i*omega*C2 + 1./(1i*omega*L2) 1];
    A3 = [1 1i*omega*L3 + 1./(1i*omega*C3);0 1];
    A4 = [1 0;1i*omega*C2 + 1./(1i*omega*L2) 1];
    A5 = [1 0;1/g5 1];
    A = A0*A1*A2*A3*A4*A5;
    H(i) = 2./(A(1));
end


figure(1);
plot(freq/1e9,-20*log10(abs(H)));hold on
scatter(fL/1e9,3,400,'.');hold on
scatter(fU/1e9,3,400,'.');hold on
scatter(5.2,40,400,'.');hold on
xlabel('频率 Ghz'); 
ylabel('衰减 dB');
grid on;
ylim([-3 50]);
N = 4;
title([num2str(N),'阶带通滤波器']);

function IL = insertion_loss(RPL,N,omega)
a = sqrt(10^(RPL/10)-1);
if abs(omega)<=1
    T = cos(N*acos(omega));
else
    T = cosh(N*acosh(omega));
end
IL = 10*log10(1+a*a*T*T);
end
