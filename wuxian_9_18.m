clear
close all
clc

S11 = 0.89*exp(1i*(-61)/180*pi);
S12 = 0.02*exp(1i*(62)/180*pi);
S21 = 3.12*exp(1i*(124)/180*pi);
S22 = 0.78*exp(1i*(-28)/180*pi);
deta = S11*S22-S12*S21;
abs_deta = abs(deta)
k = (1-abs(S11)^2-abs(S22)^2+abs(deta)^2)/(2*abs(S12)*abs(S21))
mu = (1-abs(S11)^2)/(abs(S22-conj(S11)*deta)+abs(S21*S12))

addpath(genpath('E:\zzz\learning\��Ƶ\RF_matlab'))

smith_chart; 
s_param=[S11,S12;S21,S22];
% ����ȶ���
% [K,delta] = K_factor(s_param)
input_stability(s_param, 'r');
output_stability(s_param, 'b');
