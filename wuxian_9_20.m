clear
close all
clc

S11 = 0.5*exp(1i*(-60)/180*pi);
S12 = 0.02*exp(1i*(0)/180*pi);
S21 = 6.5*exp(1i*(115)/180*pi);
S22 = 0.6*exp(1i*(-35)/180*pi);
deta = S11*S22-S12*S21;
abs_deta = abs(deta)
k = (1-abs(S11)^2-abs(S22)^2+abs(deta)^2)/(2*abs(S12)*abs(S21))
mu = (1-abs(S11)^2)/(abs(S22-conj(S11)*deta)+abs(S21*S12))

%% 
GSmax = 1/(1-abs(S11)^2)
GLmax = 1/(1-abs(S22)^2)
G0 = abs(S21)^2
GTUmax = GSmax*GLmax*G0

GSmax_dB = 10*log10(GSmax)
GLmax_dB = 10*log10(GLmax)
G0_dB = 10*log10(G0)
GTUmax_dB = 10*log10(GTUmax)
GTUmax_dB = GSmax_dB+GLmax_dB+G0_dB

U = abs(S11)*abs(S12)*abs(S21)*abs(S22)/(1-abs(S11)^2)/(1-abs(S22)^2)
(1+U)^(-2)
(1-U)^(-2)

GammaS = conj(S11);
GammaL = conj(S22);
% 由(9.8)可知，转换功率增益
GT = (1-abs(GammaL)^2) * (abs(S21)^2) * (1-abs(GammaS)^2)/...
    ((abs( (1-S11*GammaS) * (1-S22*GammaL) - S21*S12*GammaL*GammaS ))^2)
GT_dB = 10*log10(GT)





