close all; 
clear 
clc
addpath(genpath('E:\zzz\learning\射频\RF_matlab'))

smith_chart; % create a Smith Chart

global Z0;
Set_Z0(50);

% 晶体管的S参数
s11=0.6*exp(1i*(-60)/180*pi);
s12=0.05*exp(1i*(26)/180*pi);
s21=1.9*exp(1i*(81)/180*pi);
s22=0.5*exp(1i*(-60)/180*pi);
s_param=[s11,s12;s21,s22];

% 晶体管的噪声参数
% 最小噪声系数 dB
Fmin_dB=1.6;
% 最小噪声系数
Fmin=10^(Fmin_dB/10);
% 等效噪声电阻
Rn=20;
% 最佳源反射系数
Gopt=0.62*exp(1i*100/180*pi);

% 检查稳定性
[K,delta] = K_factor(s_param)

% compute a noise circle
% desired noise performance
Fk_dB=2; 
Fk=10^(Fk_dB/10);

% 由(9.79)可知，
Qk=abs(1+Gopt)^2*(Fk-Fmin)/(4*Rn/Z0);
% 由(9.83)可知，等噪声系数圆圆心
dfk=Gopt/(1+Qk); 
% 由(9.84)可知，等噪声系数圆半径
rfk=sqrt((1-abs(Gopt)^2)*Qk+Qk^2)/(1+Qk); 

% 等噪声系数圆
a=[0:360]/180*pi;
hold on;
plot(real(dfk)+rfk*cos(a),imag(dfk)+rfk*sin(a),'b','linewidth',2);
text(real(dfk)-0.1,imag(dfk)+rfk+0.08,...
   strcat('\bfF_k=',sprintf('%g',Fk_dB),'dB'));
% 最佳反射系数
plot(real(Gopt),imag(Gopt),'bo');
text(real(Gopt)+0.05,imag(Gopt)+0.05,'\bf\Gamma_{opt}');
text(real(Gopt)+0.05,imag(Gopt)-0.05,...
   strcat('\bfF_{min}=',sprintf('%g',Fmin_dB),'dB'));







% % specify the desired gain
% G_goal_dB=8;
% G_goal=10^(G_goal_dB/10);
% 
% % find the constant operating power gain circles
% delta=det(s_param);
% go=G_goal/abs(s21)^2; % normalized the gain
% dgo=go*conj(s22-delta*conj(s11))/(1+go*(abs(s22)^2-abs(delta)^2)); % center
% 
% rgo=sqrt(1-2*K*go*abs(s12*s21)+go^2*abs(s12*s21)^2);
% rgo=rgo/abs(1+go*(abs(s22)^2-abs(delta)^2)); % radius
% 
% % map a constant gain circle into the Gs plane
% rgs=rgo*abs(s12*s21/(abs(1-s22*dgo)^2-rgo^2*abs(s22)^2));
% dgs=((1-s22*dgo)*conj(s11-delta*dgo)-rgo^2*conj(delta)*s22)/(abs(1-s22*dgo)^2-rgo^2*abs(s22)^2);
% 
% % plot a constant gain circle in the Smith Chart
% hold on;
% plot(real(dgs)+rgs*cos(a),imag(dgs)+rgs*sin(a),'r','linewidth',2);
% text(real(dgs)-0.1,imag(dgs)-rgs-0.05,...
%    strcat('\bfG=',sprintf('%g',G_goal_dB),'dB'));
% 
% % choose a source reflection coefficient Gs
% Gs=dgs+1i*rgs;
% plot(real(Gs), imag(Gs), 'ro');
% text(real(Gs)-0.05,imag(Gs)+0.08,'\bf\Gamma_S');
% 
% % find the actual noise figure
% F=Fmin+4*Rn/Z0*abs(Gs-Gopt)^2/(1-abs(Gs)^2)/abs(1+Gopt)^2;
% % print out the actual noise figure
% Actual_F_dB=10*log10(F)
% 


