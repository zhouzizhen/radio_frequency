clear
close all
clc

S11 = 0.94*exp(1i*(164)/180*pi);
S12 = 0.03*exp(1i*(60)/180*pi);
S21 = 1.22*exp(1i*(43)/180*pi);
S22 = 0.57*exp(1i*(-165)/180*pi);
deta = S11*S22-S12*S21;
abs_deta = abs(deta)
k = (1-abs(S11)^2-abs(S22)^2+abs(deta)^2)/(2*abs(S12)*abs(S21))
mu = (1-abs(S11)^2)/(abs(S22-conj(S11)*deta)+abs(S21*S12))



